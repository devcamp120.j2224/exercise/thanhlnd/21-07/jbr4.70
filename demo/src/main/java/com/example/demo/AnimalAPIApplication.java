package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnimalAPIApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnimalAPIApplication.class, args);
	}

}
