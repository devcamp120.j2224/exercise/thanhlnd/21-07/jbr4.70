package com.example.demo.Model;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerAnimal {
    @GetMapping("/cat")
    public ArrayList<Cat> getListCat(){
        ArrayList<Cat> catList = new ArrayList<Cat>();
        
        Cat cat1 = new Cat("mèo anh");
        Cat cat2 = new Cat("mèo tam thể");
        Cat cat3 = new Cat("mèo ba tư");

        catList.add(cat1);
        catList.add(cat2);
        catList.add(cat3);

        return catList ;
    }
    @GetMapping("/dog")
    public ArrayList<Dog> getListDog(){
        ArrayList<Dog> dogList = new ArrayList<Dog>();

        Dog dog1 = new Dog("corgi");
        Dog dog2 = new Dog("dashmunch");
        Dog dog3 = new Dog("alaska");
        Dog dog4 = new Dog() ;   
        dog4.greets(new Dog("Hmong Dog"));

    
        dogList.add(dog1);
        dogList.add(dog2);
        dogList.add(dog3);


        return dogList ;
    }
}
